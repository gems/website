= Package GEMS
:sectnums:
:toc:

[horizontal]
Package name:: GEMS

== Package documentation

GEMS (Genetically Evolving Models in Science) implementation, 
    consisting of functions to help construct task and model definitions, create 
    models using genetic programming, and analyse the resulting models.

    documentation:: https://gems.codeberg.page/software


== Classes

[[individual-class]]
.INDIVIDUAL

Super classes:: STRUCTURE-OBJECT SLOT-OBJECT T 

Slots::

* `tree`
* `fitness`
* `extras`
* `eval-p`

//-

Documentation::
NIL


[[node-class]]
.NODE

Super classes:: STRUCTURE-OBJECT SLOT-OBJECT T 

Slots::

* `label`
* `entries`
* `children`

//-

Documentation::
NIL


== Functions

[[best-fitness-in-generation-function]]
.BEST-FITNESS-IN-GENERATION

Lambda list:: best-fitness-in-generation ( *generation* )

Documentation::
* `generation` - a list of individuals

Returns the best (smallest) fitness value of the given individuals.

Error:: if `generation` is an empty list.



[[best-individuals-in-generation-function]]
.BEST-INDIVIDUALS-IN-GENERATION

Lambda list:: best-individuals-in-generation ( *generation* &optional *tolerance* )

Documentation::
* `generation` - a list of individuals
  * `tolerance` - for equality in floating point comparison

  Returns the best individuals in given generation, all those whose fitness is 
  within `tolerance` of that returned by <<best-fitness-in-generation-function>>.

  Error:: if `generation` is an empty list.



[[build-experiment-function]]
.BUILD-EXPERIMENT

Lambda list:: build-experiment ( *experiment-filename* &key *task-directory* *operators* *protocol* *selection* )

Documentation::
* `experiment-filename` - name of file to write to
  * `task-directory` - directory where file should be written to
  * `operators` - folder of operators
  * `protocol` - name of the protocol file
  * `selection` - one of (:all, :basic, :ask)

  Builds the experiment definition file from given data.



[[clean-individuals-function]]
.CLEAN-INDIVIDUALS

Lambda list:: clean-individuals ( *individuals* *run-experiment* )

Documentation::
Return new list of individuals, with duplicates and dead-code removed.



[[combine-loggers-function]]
.COMBINE-LOGGERS

Lambda list:: combine-loggers ( *logger-1* *logger-2* )

Documentation::
* `logger-1` - logger function
  * `logger-2` - logger function

  Returns a new combined logger, which calls each of the given 
  loggers in turn.



[[dated-log-name-function]]
.DATED-LOG-NAME

Lambda list:: dated-log-name ( )

Documentation::
Returns a log name using the current date/time, e.g. log-yyyymmdd-hhmmss.csv.
This is often useful for creating temporary log files.



[[find-best-worst-pairs-function]]
.FIND-BEST-WORST-PAIRS

Lambda list:: find-best-worst-pairs ( *individuals* )

Documentation::
* `individuals` is a list of <<individual-class>> instances

  Returns a pair of pairs: the first pair being the two most similar individuals, and 
  the second pair being the two most dissimilar individuals. Similarity is measured 
  using <<similarity-function>>.



[[half-sigmoid-function]]
.HALF-SIGMOID

Lambda list:: half-sigmoid ( *x* )

Documentation::
Rescales [0.5,1.0] output from sigmoid to [0.0,1.0]



[[individual-extras-function]]
.INDIVIDUAL-EXTRAS

Lambda list:: individual-extras ( *instance* )

Documentation::
NIL



[[individual-fitness-function]]
.INDIVIDUAL-FITNESS

Lambda list:: individual-fitness ( *instance* )

Documentation::
NIL



[[individual-p-function]]
.INDIVIDUAL-P

Lambda list:: individual-p ( *object* )

Documentation::
NIL



[[individual-tree-function]]
.INDIVIDUAL-TREE

Lambda list:: individual-tree ( *instance* )

Documentation::
NIL



[[init-gems-function]]
.INIT-GEMS

Lambda list:: init-gems ( *taskname* *protocol* &key *selection* )

Documentation::
* `taskname` - the task or experiment name
  * `protocol` - name of protocol file
  * `selection` - one of `(:all :basic :ask)` - defaults to `:all`

  Carries out several steps to create the operators and protocol definitions.



[[launch-function]]
.LAUNCH

Lambda list:: launch ( *operator-set* *fitness-function* &key *total-generations* *population-size* *initial-depth* *maximum-depth* *elitism* *type* *logger* *t-size* *crossover-rate* *mutation-rate* )

Documentation::
Start GP process



[[make-individual-function]]
.MAKE-INDIVIDUAL

Lambda list:: make-individual ( &key *(tree tree)* *(fitness fitness)* *(extras extras)* *(eval-p eval-p)* )

Documentation::
NIL



[[make-logger-function]]
.MAKE-LOGGER

Lambda list:: make-logger ( *filename* &key *name* *kind* *header* *filter* *if-exists* )

Documentation::
* `filename` - name of file to save report in
  * `name` - name of experiment to record in long-form report
  * `kind` - type of report to use - one of (`:log` `:trace` or a function)
  ** `:log` - reports on generation, fitness, extras in CSV format
  ** `:trace` - produces information on every individual in the population
  in a file which can be read back using gpstats <<read-trace-function>>.
  ** function - takes arguments (str generation-number population) where
  *** `str` - stream to write report to
  *** `generation-number` - the current generation number
  *** `population` - an array of individuals in current population
  * `header` - a list of column headings for a :log style of output
  * `filter` - function returns t/nil for a generation number, used to 
  select which generations the report is generated for.
  * `if-exists` - parameter passed to `with-open-file` - use `:supersede` to replace
  an existing file or `:append` (default value) to append output to an existing file.

  Returns a function which is used in mini-gp to report results to given filename.



[[new-task-function]]
.NEW-TASK

Lambda list:: new-task ( *task-directory* )

Documentation::
Creates a new task, with a template for the protocol and operators



[[node-children-function]]
.NODE-CHILDREN

Lambda list:: node-children ( *instance* )

Documentation::
NIL



[[node-entries-function]]
.NODE-ENTRIES

Lambda list:: node-entries ( *instance* )

Documentation::
NIL



[[node-label-function]]
.NODE-LABEL

Lambda list:: node-label ( *instance* )

Documentation::
NIL



[[node-p-function]]
.NODE-P

Lambda list:: node-p ( *object* )

Documentation::
NIL



[[program-depth-function]]
.PROGRAM-DEPTH

Lambda list:: program-depth ( *program* )

Documentation::
Finds the depth of given program



[[program-similarity-function]]
.PROGRAM-SIMILARITY

Lambda list:: program-similarity ( *tree-1* *tree-2* )

Documentation::
Returns the similarity of two parse trees, based on sets of components of each tree. Computed using Jaccard index.



[[program-size-function]]
.PROGRAM-SIZE

Lambda list:: program-size ( *program* )

Documentation::
Counts the terms in given program



[[proportion-dead-code-function]]
.PROPORTION-DEAD-CODE

Lambda list:: proportion-dead-code ( *tree* *run-experiment* )

Documentation::
Returns the proportion of dead nodes in the tree



[[read-log-function]]
.READ-LOG

Lambda list:: read-log ( *filename* )

Documentation::
* `filename` is the name of a log file (in CSV format)

  Reads the given log file, and returns a list of lists, with each child list representing a line of numbers from the log file.



[[read-similarity-file-function]]
.READ-SIMILARITY-FILE

Lambda list:: read-similarity-file ( *filename* )

Documentation::
* `filename` - name of file to read

Reads the given dat file, and returns an array containing the similarity information.



[[read-trace-function]]
.READ-TRACE

Lambda list:: read-trace ( *filename* )

Documentation::
* `filename` - name of file to read

Reads the given trace file, and returns a list of generations, where each 
generation is in the format (generation-number individual-1 individual-2 ...).



[[redefine-function]]
.REDEFINE

Lambda list:: redefine ( *taskname* *selection* )

Documentation::
* `taskname` - the task or experiment name
  * `selection` - one of `(:all :basic :ask)`

  (Re-)Creates the operator definitions.



[[remove-dead-code-function]]
.REMOVE-DEAD-CODE

Lambda list:: remove-dead-code ( *tree* *run-experiment* )

Documentation::
Returns program tree with dead code removed.



[[replace-timeonly-code-function]]
.REPLACE-TIMEONLY-CODE

Lambda list:: replace-timeonly-code ( *tree* *run-experiment* *mappings* )

Documentation::
Use mappings to replace nodes in the code whose only purpose is to take up time.
Assumes the experiment is deterministic.



[[save-as-dot-function]]
.SAVE-AS-DOT

Lambda list:: save-as-dot ( *tree* *filename* )

Documentation::
Takes a tree and outputs a graphviz dot representation to the given filename.



[[set-initial-population-function]]
.SET-INITIAL-POPULATION

Lambda list:: set-initial-population ( *individuals* )

Documentation::
Sets the initial gp population to given list of individuals.



[[sigmoid-function]]
.SIGMOID

Lambda list:: sigmoid ( *x* )

Documentation::
Sigmoid function squashes x into range [0,1], with x=0 mapping to 0.5



[[tree-from-sexp-function]]
.TREE-FROM-SEXP

Lambda list:: tree-from-sexp ( *tree* )

Documentation::
Returns a tree of 'nodes' from a given sexp



[[tree-to-sexp-function]]
.TREE-TO-SEXP

Lambda list:: tree-to-sexp ( *tree* )

Documentation::
Returns an sexp from tree of 'nodes'



[[write-deadcode-generations-function]]
.WRITE-DEADCODE-GENERATIONS

Lambda list:: write-deadcode-generations ( *generations* *filebase* *run-experiment* )

Documentation::
* `generations` is a list of generation values, as read by `read-trace`
  * `filebase` is the first part of the filename - each generation is saved to 
  a file formed from the filebase and the generation number
  * `run-experiment` is a function to run an experiment on a given individual

  For each generation, computes the proportion of dead code in every instance,
  and then reports the frequency of each proportion under the following groups as
  a CSV file. Additionally, saves a file named "filebase-stats.csv" containing the 
  generation number, mean and standard-deviation of the dead-code proportion
  in CSV format.



[[write-fitness-generations-function]]
.WRITE-FITNESS-GENERATIONS

Lambda list:: write-fitness-generations ( *generations* *filebase* )

Documentation::
* `generations` is a list of generation values, as read by <<read-trace-function>>
  * `filebase` is the first part of the filename - each generation is saved to 
    a file formed from the filebase and the generation number

  For each generation, computes the _fitness_ similarity of every pair of instances 
  and outputs the result to a file - one file per generation - in a dat format.



[[write-similarity-generations-function]]
.WRITE-SIMILARITY-GENERATIONS

Lambda list:: write-similarity-generations ( *generations* *filebase* )

Documentation::
* `generations` is a list of generation values, as read by `read-trace`
  * `filebase` is the first part of the filename - each generation is saved to 
    a file formed from the filebase and the generation number

  For each generation, computes the _model_ similarity of every pair of instances
  and outputs the result to a file - one file per generation - in a dat format.



[[write-similarity-individuals-function]]
.WRITE-SIMILARITY-INDIVIDUALS

Lambda list:: write-similarity-individuals ( *individuals* *filename* )

Documentation::
* `individuals` is a list of <<individual-class>> instances
  * `filename` is the name of a file to save the similarity data to

  Given a list of individuals, this function computes the similarity of every pair of 
  individuals and outputs the result to a file of the given filename, in a dat format.



