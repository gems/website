= Workshop/Tutorials on Computational Scientific Discovery

+++
<p xmlns:cc="http://creativecommons.org/ns#" >Slides and other materials provided here for use by workshop/tutorial attendees are licensed under <a href="http://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-ND 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1"></a></p> 
+++

[#COGMI2024]
== Tutorial: Computational Scientific Discovery in Cognitive Science

Tutorial to be held on the 30^th^ October 2024 at CogMI-2024 Sixth IEEE 
International Conference on Cognitive Machine Intelligence, Washington DC, USA.

https://www.sis.pitt.edu/lersais/conference/cogmi/2024/tutorials.html

AI-guided scientific discovery will become an indispensable tool for scientists
in the not too distant future. This tutorial begins by introducing the area of
computational scientific discovery within the cognitive sciences along with an
overview of recent models of cognitive behaviour developed using these tools.
The second half of the tutorial presents a detailed walk-through of our
methodology and computational system, where participants will learn how to
design experiments, evolve and analyse candidate models using the GEMS
(Genetically Evolving Models in Science) system. Although specialised towards
the cognitive sciences, many of the principles of model definition and
discovery can be more broadly applied, and so the tutorial should be of
interest to the wider Cognitive Machine Intelligence community.

Chairs: Dr. Peter Lane and Prof. Fernand Gobet. (All enquiries to Peter Lane at <p.c.lane@herts.ac.uk>.)

Programme:

. Introduction to Computational Scientific Discovery in Cognitive Science, by Prof. Fernand Gobet
. Tutorial of the GEMS system, by Dr. Peter Lane
.. setting up task definitions for scientific experiments
.. defining a search space of candidate models
.. searching techniques, such as Genetic Programming.
.. visualisation and analysis of results 
. Examples of applying this and related methodologies in Cognitive Science, by Dr. Laura Bartlett and Dr. Dmitry Bennett

[#AI2023]
== Workshop: Computational Scientific Discovery in the Social Sciences

Workshop held on the 12^th^ December 2023 at AI-2023 Forty-third SGAI
International Conference on Artificial Intelligence CAMBRIDGE, ENGLAND 12-14
DECEMBER 2023.

http://bcs-sgai.org/ai2023/?section=workshops

This workshop will explore some AI approaches to developing scientific models,
particularly considering applications in social science. This area is
relatively unexplored, and presents some unique challenges, particularly as
models must address both the less-than-perfect accuracy of human behaviour as
well as capture this behaviour in time-bound environments. We will provide an
introduction and case-studies in this area and deliver a tutorial on GEMS, our
own system of scientific discovery. 

Chairs: Dr. Peter Lane and Prof. Fernand Gobet. (All enquiries to Peter Lane at <p.c.lane@herts.ac.uk>.)

Programme:

. Introduction to Computational Scientific Discovery in Social Sciences, by Prof. Fernand Gobet - https://gems.codeberg.page/downloads/AI-2023-Gobet.pdf[slides]
. Examples of applying this and related methodologies in Psychology and Social Sciences 
.. Evolving Cognitive Models of Verbal Learning, by Dr. Noman Javed and Dr. Dmitry Bennett - https://gems.codeberg.page/downloads/AI-2023-Javed-Bennett.pdf[slides]
.. Using Genetic Programming to Develop Theories in Psychology, by Dr. Laura Bartlett - https://gems.codeberg.page/downloads/AI-2023-Bartlett.pdf[slides]
. Tutorial of the GEMS system, by Dr. Peter Lane - https://gems.codeberg.page/downloads/AI-2023-Tutorial.pdf[slides]
.. setting up task definitions for scientific experiments
.. defining a search space of candidate models
.. searching techniques, such as Genetic Programming.
.. visualisation and analysis of results 


