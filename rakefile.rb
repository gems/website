# Used to create a static website for the gems-science website.
#
# To use this rakefile, you need to install asciidoctor and rouge:
#
# * `gem install asciidoctor`
# * `gem install rouge`
#
# Then use the rake tasks to first "build", "show" and "update" 
# the website.
#
# Note that the main website is built with one set of styles, and 
# the 'software' for the code with a slightly different set.

CWD = File.expand_path(File.dirname(__FILE__))
SRC = File.join(CWD, 'pages')   # location of adoc files
SOFTWARE = File.join(CWD, 'pages/software')   # location of adoc files

# -----------------------------------------------------------------------------
# Definitions for pages and accessor methods for main website

Page = Struct.new(:filename, :title, :date)

PAGES = [ Page.new('index', "Genetically Evolving Models in Science", ''),
          Page.new('overview', "Overview", ''),
          Page.new('publications', "Publications", ''),
          Page.new('workshop', "Workshop/Tutorials on Computational Scientific Discovery", ''),
]

# Return page for given page filename
def page filename
  PAGES.find {|p| p.filename == filename }
end

# -----------------------------------------------------------------------------
# Apply asciidoctor on every file in SRC+SOFTWARE to create the .html shells
# If update? is true, then first checks if .adoc file is a new file or 
# has newer time than its .html file, and only converts if so.
def make_shells(update=false)
  [SRC, SOFTWARE].each do |dir|
    Dir.chdir(dir) do
      Dir.foreach('.') do |file|
        next unless file.end_with? 'adoc'
        if update
          target = file.sub(/adoc\Z/, 'html')
          if !File.exist?(target) or File.mtime(target) < File.mtime(file)
            puts "Updating file: #{file}"
            `asciidoctor -a source-highlighter=rouge -s #{file}`
          end
        else
          puts "Converting file: #{file}"
          `asciidoctor -a source-highlighter=rouge -s #{file}`
        end
      end
    end
  end
end

# -----------------------------------------------------------------------------
#

# Returns HTML header for given page_name
# -- header should be for an actual page, in which case use title from PAGES
#    or use the page_name for title
def header page_name
  title = if page(page_name) 
            page(page_name).title
          else # for pages without entry in PAGES
            page_name
          end

  return <<END
  <html><head>
    <link rel="stylesheet" type="text/css" href="/styles/main-style.css">
    <link rel="stylesheet" type="text/css" href="/styles/syntax.css">
    <title>#{title} :: GEMS Project</title>
  </head>
  <body>
    <a class="pagetitle" href="/index.html">GEMS Project</a> <br> <br>
    <a href="/overview">Overview</a> | 
    <a href="/publications">Publications</a> |
    <a href="/software">Software</a> 
    <hr>
    <div class="content">
    <h1>#{title}</h1>
END
end

# Returns HTML footer
def footer
  "</body></html>"
end


# -----------------------------------------------------------------------------
#

def software_page_title title
  case title
  when "builder"
    "GEMS Package: builder"
  when "gems-logger"
    "GEMS Package: GEMS-Logger"
  when "gems-tk"
    "GEMS Package: GEMS/Tk"
  when "gpstats"
    "GEMS Package: gpstats"
  when "index"
    "GEMS Code Nodes"
  when "mini-gp"
    "GEMS Package: mini-gp"
  when "plotting"
    "How Do I Plot Graphs?"
  when "syntax-tree"
    "GEMS Package: syntax-tree"
  else
    "GEMS Code Notes :: #{title}"
  end
end

# Returns HTML header for given page_name
def software_header title
  return <<END
  <!DOCTYPE html lang="en"><head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="/styles/style.css">
    <link rel="stylesheet" type="text/css" href="/styles/syntax.css">
    <title>GEMS Code Notes :: #{title}</title>
  </head>
  <body class="article toc2 toc-left">
<div id="header">
<h1>#{software_page_title(title)}</h1>
<div id="toc" class="toc2">
<div id="toctitle">Contents</div>
<ul class="sectlevel1">
<li><a href="/software/index.html">Overview</a></li>
<li>
<li>GEMS Packages</li>
<li>
<ul class="sectlevel2">
<li>- <a href="/software/builder">builder</a></li>
<li>- <a href="/software/gems-logger">gems-logger</a></li>
<li>- <a href="/software/gems-tk">gems/tk</a></li>
<li>- <a href="/software/gpstats">gpstats</a></li>
<li>- <a href="/software/mini-gp">mini-gp</a></li>
<li>- <a href="/software/syntax-tree">syntax-tree</a></li>
</ul>
</li>
<li>API Reference</li>
<ul class="sectlevel2">
<li>- <a href="/software/gems-api">GEMS</a></li>
<li>- <a href="/software/gems-tk-api">GEMS/Tk</a></li>
</ul>
<li>
</li>
<li>
</ul>
</div>
</div>
    <div id="content">
END
end

# Returns HTML footer
def software_footer
  return <<END
<div id="footer">
<div id="footer-text">
Last updated #{Time.now.gmtime} - Peter Lane <a href="mailto:p.c.lane@herts.ac.uk">p.c.lane@herts.ac.uk</a>
</div></body></html>
END
end

# Works through files in SRC+SOFTWARE and makes a complete HTML page in 'site'
# attaching the header and footer.
def make_page_files 
  # -- make main pages
  Dir.foreach(SRC) do |file|
    next unless file.end_with? 'html'
    puts "make page: #{file}"
    page_name = file.gsub('.html', '')
    page_dir = if page_name == 'index'
                 'site'
               else
                 File.join('site', page_name)
               end
    Dir.mkdir(page_dir) unless Dir.exist?(page_dir)

    File.open(File.join(page_dir, 'index.html'), "w") do |site_file|
      # output header
      site_file.puts header(page_name)
      # if a dated page, output date
      if page(page_name).date != ''
        site_file.puts "#{page(page_name).date}"
      end
      # output shell
      IO.foreach(File.join(SRC, file)) do |line|
        site_file.puts line
      end
      site_file.puts "</div>" # close the main content
      # output footer
      site_file.puts footer
    end
  end
  # -- make software pages
  Dir.foreach(SOFTWARE) do |file|
    next unless file.end_with? 'html'
    puts "make page: #{file}"
    page_name = file.gsub('.html', '')
    page_dir = if page_name == 'index'
                 'site/software'
               else
                 File.join('site/software', page_name)
               end
    Dir.mkdir(page_dir) unless Dir.exist?(page_dir)

    File.open(File.join(page_dir, 'index.html'), "w") do |site_file|
      # output header
      site_file.puts software_header(page_name)
      # output shell
      IO.foreach(File.join(SOFTWARE, file)) do |line|
        site_file.puts line
      end
      site_file.puts "</div>" # close the main content
      # output footer
      site_file.puts software_footer
    end
  end
end

# Creates html pages for all the pages in the SRC directory.
# Each page.html is placed into page/index.html for page/ links!
def make_pages
  Dir.mkdir('site') unless Dir.exist?('site')
  Dir.mkdir('site/software') unless Dir.exist?('site/software')
  make_page_files 
end

# Create the sitemap.xml file, which lists all the main pages
# TODO: when released, may want to add 'software' 
def make_sitemap
  File.open('site/sitemap.xml', 'w') do |f|
    f.puts "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>"
    f.puts "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">"
    PAGES.each do |page|
      f.puts "  <url>"
      if page.filename == "index"
        f.puts "    <loc>/index.html</loc>"
      else
        f.puts "    <loc>/#{page.filename}/index.html</loc>"
      end
      f.puts "  </url>"
    end
    f.puts "</urlset>"
  end
end

# -----------------------------------------------------------------------------
# Tasks

desc 'build web site'
task :build => [:clean] do
  make_shells
  make_pages
  make_sitemap
  cp_r 'downloads', 'site'
  cp_r 'images', 'site'
  cp_r 'styles', 'site'
end

task :clean do
#  `rm #{SRC}/*.html`
#  `rm #{SOFTWARE}/*.html`
  rm_rf 'site'
end

desc 'recreate the GEMS api documentation'
task :gems_api do
  `sbcl --script build-api.lisp`
end

desc 'show web site - port 8000'
task :show do
  Dir.chdir('site') do
    `ruby -run -ehttpd . -p8000`
  end
end

# update task
# check time of .adoc vs .html and only convert if necessary
desc 'update revised or new files only'
task :update do
  make_shells true
  make_pages
  make_sitemap
  cp_r 'downloads', 'site'
  cp_r 'images', 'site'
  cp_r 'styles', 'site'
end


